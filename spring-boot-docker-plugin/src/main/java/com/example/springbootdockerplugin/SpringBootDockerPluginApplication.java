package com.example.springbootdockerplugin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SpringBootDockerPluginApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDockerPluginApplication.class, args);
	}

	@GetMapping
	public String getMessage() {
		return "welcome docker...!!";
	}

}
